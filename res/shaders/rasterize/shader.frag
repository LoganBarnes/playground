#version 410

layout( location = 0 ) in vec2 screenPos;
// layout( location = 1 ) in vec2 texCoords;


//uniform sampler2D tex;


layout( location = 0 ) out vec4 outColor;


void main( void )
{
//  outColor = texture( tex, texCoords );
  // outColor = vec4( screenPos, texCoords.y, 1.0 );
  outColor = vec4( screenPos * 0.5 + 0.5, 1.0, 1.0 );
  // outColor = vec4( 1.0 );
}
