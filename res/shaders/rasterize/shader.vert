#version 410
#extension GL_ARB_separate_shader_objects : enable


layout( location = 0 ) in vec2 inFragPos;
// layout( location = 1 ) in vec2 inTexCoords;

uniform vec2 framebufferSize = vec2( 2.0 );

layout( location = 0 ) out vec2 screenPos;
// layout( location = 1 ) out vec2 texCoords;


out gl_PerVertex
{
  vec4 gl_Position;
};


void main( void )
{
  vec2 radius = framebufferSize * 0.5;
  screenPos = ( inFragPos - radius ) / radius;

  // texCoords = inTexCoords;

  gl_Position = vec4( screenPos, -1.0, 1.0 );
  // gl_Position = vec4( inFragPos, -1.0, 1.0 );
}
