Playground
==========


Download
--------
Make sure to clone all submodules as well:

```bash
git clone --recursive https://gitlab.com/LoganBarnes/playground.git
```

If the project was already cloned without submodules use this command from the root folder:

```bash
git submodule update --init --recursive
```


Build and Run
-------------

### Unix

```bash
cd run
./unixConfigureAndBuild.sh
./bin/runPlayground --driver=<options>
```


### Windows

```batch
cd run
winConfigureAndBuild.cmd
bin\runPlayground.exe --driver=<options>
```

where ```<options>``` will be replaced with a driver name once they are implemented


Drivers
-------
TBD



