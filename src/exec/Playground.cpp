// Playground.cpp

#include <iostream>
#include <memory>

#include "shared/core/EventDriver.hpp"
#include "shared/core/World.hpp"
#include "shared/core/ImguiOpenGLIOHandler.hpp"


namespace
{

int
printUsage( const std::string errorMsg = "" )
{
  if ( !errorMsg.empty( ) )
  {
    std::cerr << "ERROR: " << errorMsg << std::endl;
  }

  std::cout << "Usage: runPlayground --driver=<option> [--driver-args]\n";
  std::cout << "Driver options:\n";
  std::cout << "\tno drivers implemented\n";
  std::cout << std::endl;

  if ( errorMsg.empty( ) )
  {
    return EXIT_SUCCESS;
  }

  return EXIT_FAILURE;

} // printUsage



}


/////////////////////////////////////////////
/// \brief main
/// \return
///
/// \author Logan Barnes
/////////////////////////////////////////////
int
main(
     const int    argc, ///< number of arguments
     const char **argv  ///< array of argument strings
     )
{
  try
  {
    //
    // create world to handle physical updates
    // and ioHandler to interface between the
    // world and the user
    //
    std::unique_ptr< shared::World > upWorld;
    std::unique_ptr< shared::IOHandler > upIO;

    //
    // pass world and ioHandler to driver
    // to manage event loop
    //
    std::unique_ptr< shared::Driver > upDriver;

    if ( argc < 2 )
    {
      return printUsage( "No driver specified" );
    }

    std::string arg( argv[ 1 ] );

    if ( arg == "-h" || arg == "--help" )
    {
      return printUsage( );
    }

    // only use --driver= arguments
    std::string prefix( "--driver=" );

    if ( arg.compare( 0, prefix.size( ), prefix ) )
    {
      return printUsage( "No driver specified" );
    }

    arg = arg.substr( prefix.size( ) );

    //
    // <insert driver name here> program
    //
    if ( arg == "<insert driver name here>" )
    {
      shared::Driver::printProjectInfo( "<insert driver name here> Driver" );

      upWorld = std::unique_ptr< shared::World >( new shared::World( ) );
      upIO    = std::unique_ptr< shared::IOHandler >( new shared::ImguiOpenGLIOHandler( *upWorld ) );

      upDriver =
        std::unique_ptr< shared::EventDriver >( new shared::EventDriver( *upWorld, *upIO ) );
    }

    if ( !upDriver )
    {
      return printUsage( "Selected driver not available" );
    }


    //
    // run program
    //
    return upDriver->exec( argc - 1, &argv[ 1 ] );
  }
  catch ( const std::exception &e )
  {
    std::cerr << "ERROR: program failed: " << e.what( ) << std::endl;
  }

  // should only reach this point after an exception
  return EXIT_FAILURE;

} // main
